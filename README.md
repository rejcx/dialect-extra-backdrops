# Dialect extra backdrops

Additional backdrops for the Dialect RPG/card game:
http://www.thornygames.com/dialect-rpg

-----

![Colonies of Gilead](coloniesofgilead.jpg)
![Linguist Lines](linguistlines.jpg)
![Survival Island](survivalisland.jpg)
![The Quest](thequest.jpg)
